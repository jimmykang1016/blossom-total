No	Feature description									Symbol��д
1	Solvent exposure features (HSEBD(the number of C��atoms in the lower half sphere))	HSEBD
2	Solvent exposure features (HSEAU(number of C��atoms in the upper sphere))		HSEAU
3	Solvent exposure features (HSEAD(number of C��atoms in the lower sphere) )		HSEAD
4	Solvent exposure features (HSEBU(the number of C��atoms in the upper sphere))		HSEBU
5	Solvent exposure features (CN(coordination number))					CN
6	Solvent exposure features (RD(residue depth))						RD
7	Solvent exposure features (RDa(C��atom depth))						Rda
8	Hot spots or non-hot spots								1/-1
